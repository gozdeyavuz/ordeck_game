﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

   public GameObject[] ordecks;
    public Vector3 spawnValue;
    
    public int ordeckCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;
    public GUIText scoreText;
    public GUIText gameOverText;
    public GUIText restartText;
    int score;
    private bool gameOver;
    private bool restart;
    

 


  



    void Start () {
      
        gameOver = false;
        restart = false;
        restartText.text = "";
        gameOverText.text = "";

           
        StartCoroutine(spawnWave());
        
        

        score = 0;
        UpdateScore();
       

    }
	
	
	void Update () {

        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {

                Application.LoadLevel(Application.loadedLevel);
            }

        }

    }

    IEnumerator spawnWave()


    {
        yield return new WaitForSeconds(startWait);

        while (true)
        {
            for (int i = 0; i < ordeckCount; i++)
            {
                GameObject ordeck = ordecks[UnityEngine.Random.Range(0, ordecks.Length)];
                Vector3 spawnPosition = new Vector3(spawnValue.x, spawnValue.y, spawnValue.z);
                //Quaternion spawnRotation = new Quaternion();
                Instantiate(ordeck,spawnPosition,transform.rotation);
         


                yield return 0;



                yield return new WaitForSeconds(spawnWait);



            }
            yield return new WaitForSeconds(waveWait);

            if (gameOver)
            {
                restartText.text = "Press 'R' to restart the game";
                restart = true;
                break;

            }

        }

     
    }


  

    


    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();


    }
    void UpdateScore()
    {

        scoreText.text = "Score :" + score;
        


    }
    public void GameOver()
    {
        gameOverText.text = "Game Over";
        gameOver = true;

    }

}
