﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

     int speed  = 30;
    Rigidbody rb;


    private void Start()
    {
        //Debug.Log(gameObject.tag);
        rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector3(speed,0,0);
    }

}
