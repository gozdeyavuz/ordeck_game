﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    float target = 150.0F;
    float target2 = 0.0F;
    float speed = 270.0F;
    private float angle;
    private float angle2;
     float onMoveInterval=0.2F;
    bool position;
    void Start()
    {
        position = false;
     
        StartCoroutine(RotateOrdecks());

    }



   public IEnumerator RotateOrdecks()
    {
        

        while (true)
        {
           
            if (position == false)
            {

                yield return new WaitForSeconds(onMoveInterval);
                angle = Mathf.MoveTowardsAngle(transform.eulerAngles.x, target, speed * Time.deltaTime);
                Vector3 vectorUp = new Vector3(angle, 90, 0);
                transform.eulerAngles = vectorUp;

                yield return new WaitForSeconds(onMoveInterval);
                position = true;

            }
         
            else if(position == true)
            {
                yield return new WaitForSeconds(onMoveInterval);
                angle2 = Mathf.MoveTowardsAngle(transform.eulerAngles.x, target2, speed * Time.deltaTime);
                Vector3 vectorDown = new Vector3(angle2, 90, 0);
                transform.eulerAngles = vectorDown;

                yield return new WaitForSeconds(onMoveInterval);


                position = false;


            }




        }



    }



}
