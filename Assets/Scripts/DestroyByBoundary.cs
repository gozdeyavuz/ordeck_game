﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByBoundary : MonoBehaviour {

    public bool destroyed = false;
    private void OnTriggerExit(Collider other)
    {
        Debug.Log(other.tag);
        if (other.tag == "ordeck")
        {
            Destroy(other.gameObject);
            destroyed = true;
        }
    }
}
